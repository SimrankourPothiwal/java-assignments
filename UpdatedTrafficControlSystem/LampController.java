package model;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class LampController {
	private Lamp currentLamp;

	public LampController() {
		// get the green light starting from the East;
		currentLamp = Lamp.East;
		currentLamp.light();
		System.out.println(currentLamp + " notified");

		/*
		 * Every 10 seconds the lights turned red, and let the next direction of the
		 * light turns green
		 */
		ScheduledExecutorService timer = Executors.newScheduledThreadPool(4);
		timer.scheduleAtFixedRate(new Runnable() {
			public void run() {
				synchronized (this) {
					this.notify();
					System.out.println("Switching Direction");
					currentLamp = currentLamp.blackOut();
				}
			}
		}, 10, 10, TimeUnit.SECONDS);
	}
}