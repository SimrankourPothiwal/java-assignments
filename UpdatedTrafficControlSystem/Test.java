package model;

public class Test {

	public static void main(String[] args) {

		/* The 4 directions of the road */
		String[] directions = new String[] { "East", "West", "North", "South" };
		for (int i = 0; i < directions.length; i++) {
			new Road(directions[i]);
		}

		/* The traffic light system */
		new LampController();
	}
}
