package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Each Road object represents a route, a total of 4 routes,So the system
 * generates a total of 4 instances of Road objects. Each route randomly add new
 * vehicles, added to a collection of vehicles. Each route every second will
 * check the line of control lamp is green, is the collection of the route to
 * save vehicle in the first vehicle and then to remove through the
 * intersection.
 */
public class Road {
	private List<String> vechicles = new ArrayList<String>();

	private String name;


	public Road(String name) {
		this.name = name;

		
		// Process random and constant simulation of vehicles  on the road
		ExecutorService pool = Executors.newSingleThreadExecutor();
		pool.execute(new Runnable() {

			public void run() {
				for (int i = 1; i < 1000; i++) {
					try {
						Thread.sleep((new Random().nextInt(10) + 1) * 1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					vechicles.add(Road.this.name + "_" + i);
				}
			}

		});

		// Every second check the corresponding lamp is green, it is the release of a vehicle
		ScheduledExecutorService timer = Executors.newScheduledThreadPool(1);
		timer.scheduleAtFixedRate(new Runnable() {
			public void run() {
				if (vechicles.size() > 0) {
					boolean lighted = Lamp.valueOf(Road.this.name).isLighted();
					if (lighted) {
						System.out.println(vechicles.remove(0) + " vehicle is passing by..");
					}
				}

			}
		}, 10, 10, TimeUnit.SECONDS);

	}
}