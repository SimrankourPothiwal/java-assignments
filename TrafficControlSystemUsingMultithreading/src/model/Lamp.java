package model;

public enum Lamp {
	East, West, North, South;
	private boolean lighted;
	public boolean isLighted() {
		return lighted;
	}

	public void light() {
		this.lighted = true;
		System.out.println(name() + " lamp is green, Vehicle goes");
	}

	public Lamp blackOut() {
		this.lighted = false;
		// Returns the next light, so the light will light
		Lamp nextLamp = getNext();
		System.out.println("The green light from the " + name() + " switch " + nextLamp);
		nextLamp.light();
		return nextLamp;
	}

	public Lamp getNext() {
		Lamp[] nextLamp = Lamp.values();
		int i = 0;
		for (; nextLamp[i] != this; i++)
			;
		i++;
		i %= nextLamp.length;
		return nextLamp[i];
	}
}

	
