import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JdbcDemo {
	public static void main(String[] args) throws Exception
	{
	//Load and register the driver
	 Class.forName("oracle.jdbc.driver.OracleDriver");   //method which will load the driver
	 //Establish the connection
	 //getConnection:gives the object of connection.
	 Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "55555");
	//Create a statement
	 Statement statement=connection.createStatement();
	//Execute sql query
	//ResultSet:hold the table
	 ResultSet rs=statement.executeQuery("select * from Employee");
	//Process the result set
	 while(rs.next())        //shift the pointer and check weather next line is available
	 {
		 System.out.print(rs.getInt(1)+"\t");
		 System.out.print(rs.getString(2)+"\t");
		 System.out.print(rs.getInt(3)+"\t");
		 System.out.println();
	 }
	 statement.close();
	 connection.close();
	}

}
//oracle.jdbc.driver.OracleDriver.class
