package usingSavepoints;

//STEP 1. Import required packages
import java.sql.*;

public class SavepointRunner {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
	static final String DB_URL = "jdbc:oracle:thin:@localhost:1521:xe";

	// Database credentials
	static final String USER = "system";
	static final String PASSWORD = "55555";

	public static void printResultSet(ResultSet rs) throws SQLException {
		System.out.println("Emp_Id\tEmp_Name\t\tSalary");

		while (rs.next()) {
			// Retrieve by column name
			int id = rs.getInt(1);
			String name = rs.getString(2);
			int salary = rs.getInt(3);

			// Display values
			System.out.println(id + "\t" + name + "\t\t" + salary);
		}
	}

	public static void main(String[] args) {
		Connection connection = null;
		Statement statement = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);

			// STEP 4: Set auto commit as false.
			connection.setAutoCommit(false);

			// STEP 5: Execute a query to delete statment with
			// required arguments for RS example.
			System.out.println("Creating statement...");
			statement = connection.createStatement();

			// STEP 6:list all the available records.
			String query = "SELECT * FROM Employee";
			ResultSet rs = statement.executeQuery(query);
			System.out.println("Employee Record Table");
			printResultSet(rs);

			// STEP 7: delete rows
			Savepoint savepoint1 = connection.setSavepoint("ROWS_DELETED_1");
			System.out.println("Deleting record with ID=119");
			String query1 = "DELETE FROM Employee WHERE emp_id = 119";
			statement.executeUpdate(query1);
			// we deleted wrong employees!
			// STEP 8: Rollback the changes after save point 1.
			connection.rollback(savepoint1);

			// STEP 9: delete rows having ID
			Savepoint savepoint2 = connection.setSavepoint("ROWS_DELETED_2");
			System.out.println("Deleting record with ID=118");
			query1 = "DELETE FROM Employee WHERE emp_id = 118";
			statement.executeUpdate(query1);
			connection.rollback(savepoint2);

			// STEP 10: Now list all the available records.
			rs = statement.executeQuery(query);
			System.out.println("Employee Record Table");
			printResultSet(rs);

			// STEP 10: Clean-up environment
			rs.close();
			statement.close();
			connection.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
			// If there is an error then rollback the changes.
			System.out.println("Rolling back data here....");
			try {
				if (connection != null)
					connection.rollback();
			} catch (SQLException se2) {
				se2.printStackTrace();
			} // end try

		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (statement != null)
					statement.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
	}// end main

}// end class