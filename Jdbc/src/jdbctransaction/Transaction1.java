package jdbctransaction;

import java.sql.*;

public class Transaction1 {
	Connection connection;
	Statement statement;

	public Connection getConnection() {
		return connection;
	}

	public Transaction1() throws Exception {
		connection = MyConnection.getMyConnection();
		System.out.println("Connection Establish");
	}

	public void insertData() throws SQLException {
		statement = connection.createStatement();
		statement.executeUpdate("insert into Employee values(136,'john',70000)");
		System.out.println("Record Inserted");
	}

	public void getData() throws SQLException {
		statement = connection.createStatement();
		ResultSet result = statement.executeQuery("select * from Employee");
		while (result.next()) {
			System.out.println(result.getInt(1) + "\t" + result.getString(2) + "\t" + result.getInt(3));
		}
	}

	public static void main(String[] args) throws Exception {
		Transaction1 transaction = new Transaction1();
		transaction.getConnection().setAutoCommit(false);
		transaction.insertData();

		//transaction.getConnection().commit();
		transaction.getConnection().rollback();
		transaction.getData();


		transaction.getConnection().close();
	}

}
