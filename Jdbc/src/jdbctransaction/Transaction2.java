package jdbctransaction;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Transaction2 {
	Connection connection;
	Statement statement;

	public Connection getConnection() {
		return connection;
	}

	public Transaction2() throws Exception {
		connection = MyConnection.getMyConnection();
		System.out.println("Connection Establish");
		connection.setAutoCommit(false);
	}

	public void getData() throws SQLException {
		statement = connection.createStatement();
		ResultSet result = statement.executeQuery("select * from Employee");
		while (result.next()) {
			System.out.println(result.getInt(1) + "\t" + result.getString(2) + "\t" + result.getInt(3));
		}
	}

	public static void main(String[] args) throws Exception {
		Transaction2 transaction = new Transaction2();
		PreparedStatement ps = transaction.getConnection().prepareStatement("insert into Employee values(?,?,?)");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String opt;

		do {
			System.out.println("1.ADD record\n2.Display Record");
			String choice1 = br.readLine();
			int ch1 = Integer.parseInt(choice1);
			switch (ch1) {
			case 1:
				while (true) {
					System.out.println("Enter Id:");
					String stringId = br.readLine();
					int id = Integer.parseInt(stringId);

					System.out.println("Enter name:");
					String name = br.readLine();

					System.out.println("Enter salary:");
					String stringSalary = br.readLine();
					int salary = Integer.parseInt(stringSalary);

					System.out.println("Press\n 1.Commit\n2.rollback");
					String choice = br.readLine();
					int ch = Integer.parseInt(choice);

					switch (ch) {
					case 1:
						ps.setInt(1, id);
						ps.setString(2, name);
						ps.setInt(3, salary);
						ps.executeUpdate();
						transaction.getConnection().commit();
						System.out.println("Record commited");
						break;

					case 2:
						transaction.getConnection().rollback();
						System.out.println("Record rollback");
						break;
					default:
						System.out.println("Enter valid input");
					}

					System.out.println("Wants to add more records press(y/n)");
					String ans = br.readLine();
					if (ans.equals("n")) {
						break;
					}
				}
				break;
			case 2:
				transaction.getData();
				break;
			default:
				System.out.println("Enter valid input");
			}
			System.out.println("To continue press(y/n)");
			opt = br.readLine();
		} while (opt.equals("y"));

		transaction.getConnection().commit();
		System.out.println("Records Saved");
		transaction.getConnection().close();

	}

}
