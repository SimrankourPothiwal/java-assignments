package statementRunner;

import java.sql.*;

public class FunctionRunner {
	Connection connection;
	CallableStatement collableStatement;

	public FunctionRunner() throws Exception {
		connection = MyConnection.getMyConnection();
		System.out.println("Connection Established");
	}

	public void call() throws SQLException {
		CallableStatement collableStatement = connection.prepareCall("?= call ADD(?,?)");
		collableStatement.setInt(2, 10);
		collableStatement.setInt(3, 20);
		collableStatement.registerOutParameter(1, Types.INTEGER);// registers the output parameter with its
																	// corresponding type
		collableStatement.execute();
		System.out.println(collableStatement.getInt(1));

	}

	public static void main(String[] args) throws Exception {
		FunctionRunner add = new FunctionRunner();
		add.call();
	}

}
