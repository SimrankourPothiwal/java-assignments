package statementRunner;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class MyConnection {
	private static Connection connection;

	static Connection getMyConnection() throws Exception
	{
		if (connection==null) 
		{
			Properties dataProperty=new Properties();
			dataProperty.load(new FileInputStream(new File("C:\\Users\\simarn.kour\\eclipse-workspace\\JdbcRunner\\src\\statementRunner\\DataSource.properties")));
			String driver = dataProperty.getProperty("driver");
			String url = dataProperty.getProperty("url");
			String username = dataProperty.getProperty("username");
			String password = dataProperty.getProperty("password");
			Class.forName(driver);
			connection=DriverManager.getConnection(url, username, password);
			return connection;
		}
		return connection;
	}

}
