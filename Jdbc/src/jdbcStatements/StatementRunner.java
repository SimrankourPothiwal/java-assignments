package statementRunner;

import java.sql.*;

public class StatementRunner {
	Connection connection;
	Statement statement;
	ResultSet resultSet;

	public StatementRunner() throws Exception {
		connection = MyConnection.getMyConnection();
		System.out.println("Connection Established");
	}

	public void getData() throws Exception {
		statement = connection.createStatement();
		resultSet = statement.executeQuery("select * from Employee");
		while (resultSet.next()) {
			System.out.print(resultSet.getInt("emp_id") + "\t");
			System.out.print(resultSet.getString("emp_name") + "\t");
			System.out.print(resultSet.getString("salary") + "\t");
			System.out.println();

		}
		statement.close();
		resultSet.close();
	}

	public static void main(String[] args) throws Exception {
		StatementRunner statement = new StatementRunner();
		statement.getData();

	}

}
