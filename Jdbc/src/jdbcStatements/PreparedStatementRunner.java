package statementRunner;

import java.sql.*;

public class PreparedStatementRunner {
	Connection connection;
	PreparedStatement preparedStatement;
	ResultSet result;

	public PreparedStatementRunner() throws Exception {
		connection = MyConnection.getMyConnection();
		System.out.println("Connection Established");
	}

	public void getData() throws Exception {
		preparedStatement = connection.prepareStatement("select * from Employee where salary between ? and ?");
		preparedStatement.setInt(1, 10000);
		preparedStatement.setInt(2, 50000);
		result = preparedStatement.executeQuery();
		System.out.println("Emp_Id\tEmp_Name\tSalary");
		while (result.next()) {
			System.out.print(result.getInt(1) + "\t");
			System.out.print(result.getString(2) + "\t");
			System.out.print(result.getInt(3) + "\t\t");
			System.out.println();

		}
		preparedStatement.close();
		result.close();

	}

	public void insert(int id, String name, int salary) throws SQLException {
		preparedStatement = connection.prepareStatement("insert into Employee values(?,?,?)");
		preparedStatement.setInt(1, id);
		preparedStatement.setString(2, name);
		preparedStatement.setInt(3, salary);
		int noOfEntries = preparedStatement.executeUpdate();
		System.out.println(noOfEntries+" Record inserted");
	}

	public void update(String name, int id) throws SQLException {
		preparedStatement = connection.prepareStatement("update Employee set emp_name=? where emp_id=?");
		preparedStatement.setString(1, name);
		preparedStatement.setInt(2, id);
		preparedStatement.executeUpdate();
	}

	public void delete(int id) throws SQLException {
		preparedStatement = connection.prepareStatement("delete from Employee where emp_id=?");
		preparedStatement.setInt(1, id);
		preparedStatement.executeQuery();
	}

	public static void main(String[] args) throws Throwable {
		PreparedStatementRunner preparedStatementRunner = new PreparedStatementRunner();
		preparedStatementRunner.insert(108, "jassi", 50000);
		// preparedStatementRunner.insert(105, "sam", 80000);
		System.out.println("Data Before Update:");
		preparedStatementRunner.getData();
		preparedStatementRunner.update("prince", 108);
		System.out.println("Data After Update:");
		preparedStatementRunner.getData();
		preparedStatementRunner.delete(104);
		System.out.println("Data After Delete:");
		preparedStatementRunner.getData();



	}

}
