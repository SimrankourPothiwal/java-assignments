package statementRunner;

import java.sql.*;

public class ProcedureRunner {

	Connection connection;
	CallableStatement collableStatement;

	public ProcedureRunner() throws Exception {
		connection = MyConnection.getMyConnection();
		System.out.println("Connection Established");
	}

	public void call() throws SQLException {
		collableStatement = connection.prepareCall("{call INSERTRECORD(?,?,?)}");
		collableStatement.setInt(1, 130);
		collableStatement.setString(2, "jaspreet");
		collableStatement.setInt(3, 60000);
		collableStatement.execute();

		System.out.println("success");
	}

	public void getData() throws SQLException {
		Statement statement = connection.createStatement();
		ResultSet result = statement.executeQuery("select * from Employee");
		while (result.next()) {
			System.out.print(result.getInt(1) + "\t" + result.getString(2) + "\t" + result.getInt(3) + "\n");
		}

	}

	public static void main(String[] args) throws Exception {
		ProcedureRunner procedure = new ProcedureRunner();
		procedure.call();
		procedure.getData();

	}

}
